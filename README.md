# [Bayesian Methods for Machine Learning](https://www.coursera.org/learn/bayesian-methods-in-machine-learning/home/welcome)
by National Research University Higher School of Economics

## Week 1
* Introduction to Bayesian methods
* Conjugate priors

## Week 2 
* Latent Variable Models
* Expectation-Maximization algorithm
:cyclone: [Assignment:  Expectation-maximization algorithm ](week2/Coursera-BMML_-week-2.ipynb)

## Week 3
* Variational inference
* Latent Dirichlet Allocation 

## Week 4
Markov chain Monte Carlo
:link:[Assignment:  Using PyMC3](week4/Week4.%20Practical%20Assignment.%20MCMC.ipynb)

## Week 5
Variational Autoencoder
Variational Dropout
[ Variational Autoencoder](week5/assignment.ipynb):scissors:

## Week 6
Gaussian processes & Bayesian optimization
[ Gaussian processes and Bayesian optimization](week6/assignment/Coursera_BMMLweek6.ipynb):v: :raised_hand: :fist:

## Final Project  Assignment
 [Final project: Finding the suspect](week6/final/Coursera%20BMML,%20Final%20project.ipynb)

